var draw;
var tool;
var m,c;
var cPushArray = new Array();
var cStep = -1;
var choose_font;

function init(){
  cPush();
  m=document.getElementById("canvas");
  c=m.getContext("2d");
  c.rect(0,0,600,450);
  c.fillStyle="white";
  c.fill();
  c.rect(0,0,600,450);
  c.fillStyle="white";
  c.fill();
}
function md(event){
  if(tool=="pen"){
    c.moveTo(event.offsetX,event.offsetY);
    draw=true;
    c.beginPath();
    c.strokeStyle = document.getElementById("Palette").value;
  }
  if(tool=="eraser"){
    c.moveTo(event.offsetX,event.offsetY);
    draw=true;
    c.beginPath();
    c.strokeStyle="white";
  }
  if(tool=="img"){
    var img = new Image();
    img = document.getElementById("temp");
    c.drawImage(img,event.offsetX,event.offsetY,150,150);
  }
  if(tool=="text"){
    var str = document.getElementById("text").value;
    c.fillStyle = document.getElementById("Palette").value;
    c.fillText(str,event.offsetX,event.offsetY);
  }
}
function mv(event){
  if(draw){
    c.lineTo(event.offsetX,event.offsetY);
    c.stroke();
  }
}
function mup(){
  cPush();
  draw=false;
  c.closePath();
}
function changecolor(){
  c.strokeStyle = document.getElementById("Palette").value;
}
function reset(){
  c.clearRect(0,0,600,450);
  c.rect(0,0,600,450);
  c.fillStyle="white";
  c.fill();
  c.rect(0,0,600,450);
  c.fillStyle="white";
  c.fill();
  cPush();
}
function pen(){
  tool="pen";
  document.getElementById("canvas").style.cursor = "crosshair";
}
function eraser(){
  tool="eraser";
  document.getElementById("canvas").style.cursor = "cell";
}
function range(){
  c.lineWidth=document.getElementById("range").value;
}
function img(){
  tool="img";
  document.getElementById("canvas").style.cursor = "move";
}
function text(){
  tool="text";
  var size;
  size = document.getElementById("tsize").value;
  console.log(size);
  c.font = "normal 30px Arial";
  if(choose_font == "normal"){
    c.font = "normal " + size + "px " + "Arial";
  }
  else if(choose_font == "italic"){
    c.font = "italic " + size + "px " + "Arial";
  }
}
function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(document.getElementById('canvas').toDataURL());
}
function undo() {
  if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { c.drawImage(canvasPic, 0, 0); }
  }
}
function redo() {
  if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { c.drawImage(canvasPic, 0, 0); }
  }
}
function font(){
  choose_font=document.getElementById("letter").value;
}